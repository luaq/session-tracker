# SessionStats
Session stats keeps track of your Hypixel statistics per each time you run it (they're called sessions). It's configurable via the settings.json file which I will now NOT show you how to use.

## Requirements
 - You have to have Python 3.7 (yes, really)
 - Internet connection (it's not the fucking stone age)
 - JSON knowledge, please don't ask me what JSON is
 - Python requests library (`python -m pip install requests`)

 ## Setup
 To set it up, it's not rocket science! All you have to do is have the latest version of Python (this was made in 3.7.4). Git clone this with `git clone https://gitlab.com/Luaq/session-tracker/ && cd session-tracker`. Then type `pip install -e .`. Then you're all good to start `track`ing.
 