from datetime import datetime
from pathlib import Path
import requests
import json
import os


default_settings = {
    "api_key": "TO GET YOUR API KEY, LOG ON HYPIXEL AND TYPE '/API NEW'",
    "uuid": "YOUR UUID (WITH NO DASHES) https://namemc.com/",
    "updates_per_minute": 60,
    "track": [
        "player.stats.SkyWars.kills",
        "player.stats.SkyWars.deaths",
        "player.stats.SkyWars.wins",
        "player.stats.SkyWars.losses",
        {
            "key": "player.stats.SkyWars.coins",
            "method": "fullValue"
        },
        {
            "key": "player.stats.SkyWars.souls",
            "method": "fullValue"
        }
    ],
    "custom": [
        {
            "name": "kdr",
            "type": "ratio",
            "calc": ";player.stats.SkyWars.kills; / ;player.stats.SkyWars.deaths;"
        },
        {
            "name": "wlr",
            "type": "ratio",
            "calc": ";player.stats.SkyWars.wins; / ;player.stats.SkyWars.losses;"
        }
    ]
}
settings = {}
base_url = "https://api.hypixel.net"
session_start_date = datetime.today().strftime("%Y-%m-%d %H.%M.%S")
working_dir = f"{os.getenv('UserProfile', 'C:/python')}\\.stats-tracker".replace("\\", "/")


def make_call(path: str, api_key: str, *args, **kwargs):
    global base_url
    full_url = "/".join([base_url, path])  # join the path and base_url with a /
    kwargs["key"] = api_key  # set the key param
    # send the request and parse it as json
    return requests.get(full_url, kwargs).json()


def parse_settings():
    global default_settings, settings
    settings_filepath = f"{working_dir}/settings.json"
    # create the settings.json file if it doesn't exist
    if not Path(settings_filepath).exists():
        settings_path = extract_paths_from(settings_filepath)
        if not Path(settings_path).exists():
            os.makedirs(settings_path)
        with open(settings_filepath, "w") as settings_file:
            json.dump(default_settings, settings_file, sort_keys=True, indent=2)
            settings_file.close()
        settings = default_settings
        return
    # parse the settings.json into a variable
    with open(settings_filepath, "r") as settings_file:
        settings = json.load(settings_file)
        settings_file.close()


def ratio(n1: float, n2: float):
    try:
        return n1 / n2
    except ZeroDivisionError:
        return n1


def read_dict_path(path: str, dictionary: dict):
    # if there are no dots then return dictionary[path]
    if "." not in path:
        return dictionary[path] if path in dictionary else None
    sub_paths = path.split(".")  # will allow things like player.stats.SkyWars
    curr_dict = dictionary
    # loop through each subpath
    for sub_path in sub_paths:
        value = curr_dict[sub_path]
        if not value:
            return None
        # check if the value is a dictionary, if it isn't then return it
        if not isinstance(value, dict) or type(value) != dict:
            return value
        else:
            # if the value is a dictionary, then continue into the sub
            curr_dict = value
    return curr_dict  # if all fails and loop never returns anything then assume they want a dictionary


def stats_format(stats, string: str):
    found_keys = []
    # loop the paths (keys) and check the string for it
    for key in stats._start.keys():
        search = f";{key};"
        if search not in string:
            continue
        key_value = stats.calc_value(key)
        # don't know if the assignment is required, but not bouta test it
        string = string.replace(search, str(key_value))
        found_keys.append((key, key_value))  # it was found, add it (with the value that was assigned)
    return (string, found_keys)


def extract_paths_from(file_path: str):
    return "/".join(file_path.replace("\\", "/").split("/")[:-1])
