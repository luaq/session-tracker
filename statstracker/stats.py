from pathlib import Path
from . import utils
import json
import os


class Stats:
    _start = {}
    _last = {}
    _custom = {}


    def __init__(self, json_data: dict):
        self.__start_data = json_data
        self._player_uuid = json_data["player"]["uuid"]
        # loop through every track data and save its starting value
        for key in utils.settings["track"]:
            key_path = key["key"] if isinstance(key, dict) or type(key) == dict else key
            self._start[key_path] = utils.read_dict_path(key_path, json_data)


    def update(self, new_data: dict):
        changes = []
        # loop through every key in _start to grab it from new_data
        for key in self._start.keys():
            new_value = utils.read_dict_path(key, new_data)
            # check for changes
            if key not in self._last or self._last[key] != new_value:
                changes.append(key)  # changes detected
            self._last[key] = new_value
        # update the custom values (if the exist)
        custom_values: list = utils.settings["custom"]
        if type(custom_values) == list and len(custom_values) <= 0:
            return changes  # forget it, there's nothing custom
        return changes + [f"STCustom.{change}" for change in self.__update_custom_values(custom_values)]


    def calc_value(self, path: str):
        track_list = utils.settings["track"]
        if path in track_list:
            return self.session_diff(path)  # it's not a special boi
        # loop through each track
        for track_data in track_list:
            # check that it does not have special demands (and that we have the right one) o.O
            if type(track_data) != dict or track_data["key"] != path:
                continue  # ignore it mate
            track_method = track_data["method"]
            if track_method == "fullValue":
                return self._last[path]
            else:  # default to difference
                return self.session_diff(path)
            break
        return -1


    def session_diff(self, path: str):
        try:
            return int(self._last[path]) - int(self._start[path])
        except TypeError:
            return None


    def save_session_file(self, file_path: str = None):
        file_path = file_path or self.session_file_path()
        paths = utils.extract_paths_from(file_path)
        if not Path(paths).exists():
            os.makedirs(paths)  # make sure that folder exists -.-
        # open the file to write
        with open(file_path, "w") as session_file:
            final_json = {}
            # loop each setting to get its value and save it to the final json
            for track_setting in utils.settings["track"]:
                fix_name = lambda path, i: path.split(".")[-1 - i]
                key: str = track_setting if type(track_setting) != dict else track_setting["key"]
                i = 0
                name = fix_name(key, 0)
                # unique names only :(
                while name in final_json:
                    name = fix_name(key, i)
                    if i > 2 or i - 1 >= len(key.split(".")):
                        name = key
                        break
                    i += 1
                final_json[name] = self.calc_value(key)
            # loop through each custom value and set its place
            for custom_key in self._custom.keys():
                final_json[custom_key] = self._custom[custom_key]
            # send the json data to the session file
            json.dump(final_json, session_file, sort_keys=True, indent=2)
            session_file.close()


    def session_file_path(self):
        return f"{utils.working_dir}/sessions/{utils.session_start_date}.json"


    def __update_custom_values(self, dict_list: list):
        if len(dict_list) <= 0:
            return []  # there is nothing, stop wasting my time
        changes = []
        # loop each dictionary
        for custom_value in dict_list:
            if type(custom_value) != dict:
                continue  # skip those that lie
            custom_name = custom_value["name"]  # should have been added a long time ago
            custom_calc: str = custom_value["calc"]
            custom_type = custom_value["type"] if "type" in custom_value else None
            format_str, changes_list = utils.stats_format(self, custom_calc)
            # check that it's different
            try:
                solve = eval(format_str)
            except ZeroDivisionError:
                solve = 0 if custom_type != "ratio" else changes_list[0][1]
            if custom_name in self._custom and self._custom[custom_name] == solve:
                continue  # skip it because nothing is different
            changes.append(custom_name)
            if not custom_type:
                # set it
                self._custom[custom_name] = solve
                continue
            # if it has a type value, apply specific rules (behaviour changes may take place)
            if custom_type == "ratio":
                if custom_calc.count("/") != 1:
                    continue  # ratios don't work this way (not here!!!)
                if solve < 0:
                    solve = 0
                self._custom[custom_name] = solve
        return changes
