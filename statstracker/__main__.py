from .stats import Stats
from . import utils
import importlib
import time
import json


player_stats = None


def main():
    global player_stats

    # parse the settings
    utils.parse_settings()

    # alias sort of?
    settings = utils.settings

    # catch escape keys
    try:
        if "api_key" not in settings or "uuid" not in settings:
            print("The settings.json file is missing both the API key and the UUID. Explain yourself.")  # just read this and that doesn't match up with the if statement, but who cares?
            exit(0)
            return

        api_key = str(settings["api_key"])
        track_id = str(settings["uuid"]).replace("-", "")  # grab the UUID from the settings file (get rid of the dashes if necessary)

        first_call = utils.make_call("player", api_key, uuid=track_id)
        if not validate_call(first_call):
            return
        player_stats = Stats(first_call)

        updates_per_minute = settings["updates_per_minute"] if "updates_per_minute" in settings else 60
        
        # do not remove this
        # please? :(
        if updates_per_minute > 120:
            print("120 is the maximum amount of requests Hypixel's API allows per minute. You've exceeded that limit, we cannot let you proceed.")
            exit(0)
            return

        delta_time = 60.0 / int(settings["updates_per_minute"])  # calculate how often per minute it needs to update
        last_time = time.time()

        print("Tracking your stats for this script session. End the task with an escape key.")
        print_session_file()
        while True:
            # if the time passed is greater than the delta_time
            if time.time() - last_time > delta_time:
                last_time += delta_time  # increase the last_time by delta_time (so that this statement doesn't stay infinetly true)
                update_stats(api_key, player_stats)  # gets called every x times per minute
    except KeyboardInterrupt:
        print("Received exit key.")
        print_session_file()
        exit(0)


def update_stats(api_key: str, player_stats: Stats):
    response = utils.make_call("player", api_key, uuid=player_stats._player_uuid)
    if not validate_call(response):
        return
    changes = player_stats.update(response)
    if len(changes) > 0:  # things have been modified, clearly
        print("New information has been detected. Tracking changes:")
        # output all the changes
        for change in sorted(changes):
            print(f"\t{change}")
        player_stats.save_session_file()  # optimization (sort of)


def validate_call(call: dict):
    if not call["success"]:
        print("Failed to get first data, check your UUID and make sure your API key is correct.\nYou can get a new one on Hypixel by typing /api key")
        print(f"Settings file is located at:\n\t{utils.working_dir}/settings.json")
        exit(0)
        return False
    return True


def print_session_file():
    print(f"Your session's data will be located here:\n\t{player_stats.session_file_path()}")


if __name__ == "__main__":
    main()
