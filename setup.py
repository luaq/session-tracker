from setuptools import setup

setup(
    name="statstracker",
    version="1.0",
    description="Tracks your Hypixel stats via the commandline.",
    author="luaq.",
    packages=[
        "statstracker"
    ],
    entry_points={
        "console_scripts": [
            "track = statstracker.__main__:main",
            "tracker = statstracker.__main__:main"
        ]
    },
    install_requires=[
        "requests"
    ]
)